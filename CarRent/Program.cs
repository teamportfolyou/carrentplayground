using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using CarRent.DAL.DataModel;
using CarRent.Service;
using CarRent.Repository.Repositories;
using CarRent.DAL;
using CarRent.Repository;
using CarRent.Common.CacheProvider;
using CarRent.Service.Models;
using CarRent.Service.Services.EmailSender;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;

namespace CarRent
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
    }
}